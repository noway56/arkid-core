# Generated by Django 3.2.10 on 2022-03-02 06:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0039_auto_20220223_0803'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='user_permissions_group',
        ),
    ]
